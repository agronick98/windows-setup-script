# Windows Setup Script
This script is very useful for setting up new computers to join a domain, install software, and run windows updates.

## Usage
You can start `setup_script.ps1` at the login of a local administrator account. The script will prompt for credentials when required, and create scheduled tasks to resume after reboot for renaming the computer and installing Windows updates. The script will run `step 1` by default and supports the flags `-step 1`, `-step 2`, and `-step 3` to run a specific step.

## Requirements
This script has only been tested on Windows 10 1909 and Windows 10 2004. It requires the following modules which are included in the project files for your conveinience:
- [WASP](https://www.powershellgallery.com/packages/Wasp/2.5.0.0)
- [PSWindowsUpdate](https://www.powershellgallery.com/packages/PSWindowsUpdate/2.2.0.2)
- [ActiveDirectory](https://docs.microsoft.com/en-us/powershell/module/addsadministration)
