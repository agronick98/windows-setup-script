<#PSScriptInfo
.VERSION 2.3

.AUTHOR Austin Agronick

.DESCRIPTION 
 This script runs on newly imaged computers to join to the domain, reboot, and setup required software.
#> 

param([Int32]$step=1)

class Program
{
    # Properties
    [string]    $Name #name of program
    [string]    $InstallerPath #program installer path
    [string[]]  $Version = $null #version of executable(s) being installed
    [string]    $OnlyModels = $null #only install on these computer models
    [string]    $MainWindowTitle = $null #program window title to wait for installer window to close
    [string[]]  $ProgramPath = $null #path of installed executable(s)
    [string[]]  $Arguments = $null #arguments to run with installer
    [bool]      $OnlyLaptops = $false #only install program on laptops
    [bool]      $LaunchAfterInstall = $false #run program after installation
    [bool]      $WaitForCompletion = $true #wait for install to finish

    static [string] $ComputerModel = (Get-CIMInstance -classname win32_computersystem).Model

    # Install the program
    [void] install() {
        if ( ($this.OnlyModels -eq $null -or ($this.ComputerModel -match $this.OnlyModels)) -and !($this.OnlyLaptops -and $global:ComputerIsDesktop) ) {
                Write-Host "Installing" $this.Name
                $InvokeCmdParams = @{ Wait = $this.WaitForCompletion } # use splatting to support programs run with no arguments
                if ($this.arguments) {$INvokeCmdParams.ArgumentList = $this.arguments}
                Start-Process $this.InstallerPath @InvokeCmdParams

                if ($this.MainWindowTitle) {
                    while ((Get-Process | Where-Object { $_.MainWindowTitle -eq $this.MainWindowTitle }) -eq $null) {} # wait for installer to start
                    while (Get-Process | Where-Object { $_.MainWindowTitle -eq $this.MainWindowTitle }) {} # wait for installer to finish
                }
                if ($this.LaunchAfterInstall) {
                    Start-Sleep 3
                    Start-Process $this.ProgramPath[0]
                }
        } else {
            Write-Host "Skipping" $this.Name "installtion"
        }
    }

    [bool] isInstalled() {
        for ($i = 0; $i -lt $this.ProgramPath.Length; i++) {
            # if specific program version installed
            if ( (($this.Version.Length -gt 0) -and $this.Version[$i] -and ((Get-Command $this.ProgramPath[$i] -ErrorAction SilentlyContinue).FileVersionInfo.FileVersion) -eq $this.Version[$i]) -or
                 (Get-Command $this.ProgramPath[$i] -ErrorAction SilentlyContinue) # or any version installed
               ){
                    return $true 
            } else {
                return $false
            }  
        }
        return $true
    }
}

$programs = @(
    [Program]@{
        Name = "DocVelocity"
        InstallerPath = "C:\group_policy_files\DocVelocity_Desktop_v.8.0.4.94.msi" 
        ProgramPath = @("C:\Program Files (x86)\DocVelocity Desktop\Desktop\Desktop.exe")
        Arguments = @("/quiet") 
        LaunchAfterInstall = $true
    },
    [Program]@{
        Name = "D6000 Docking Station driver"
        InstallerPath = "C:\group_policy_files\First Login Applications\setup_script\DisplayLink-Driver-for-Dell-SuperSpeed-USB3-Universal_VHPDW_WIN_9.3.3324.0_10.2.3.EXE" 
        ProgramPath = @("C:\Program Files\DisplayLink Core Software\DisplayLinkTrayApp.exe")
        Arguments = @("/s")
        OnlyLaptops = $true 
    },
    [Program]@{
        Name = "Rapid Storage Driver"
        InstallerPath = "C:\group_policy_files\Intel-Rapid-Storage-Technology-Driver_1NN1D_WIN_17.2.4.1011_A02_01.EXE" 
        OnlyModels = "^Latitude (5500|5501|5401)$"
        ProgramPath = @("C:\Program Files\Intel\Intel(R) Rapid Storage Technology\Lang\resource.dll")
        Arguments = @("/s")
    },
    [Program]@{
        Name = "ITSupport247 with ITSPlatform"
        InstallerPath = "C:\group_policy_files\First Login Applications\12166_DPMA_GUI_544.exe" 
        MainWindowTitle = "ITSupport247-DPMA Setup"
        ProgramPath = @("C:\Program Files (x86)\SAAZOD\DM2.0\Platform-Agent-Exe.exe", "C:\Program Files (x86)\SAAZOD\zSCC\zCCM.exe")
        WaitForCompletion = $false
    },
    [Program]@{
        Name = "Cradlepoint"
        InstallerPath = "C:\group_policy_files\First Login Applications\setup_script\Pertino-723-4888_64.msi" 
        ProgramPath = @("C:\Program Files\Pertino\pGateway.exe")
        Arguments = @("/quiet", "API_KEY=12345")
        Version = @("2,25,4888")
    },
    [Program]@{
        Name = "EXO5"
        InstallerPath = "C:\group_policy_files\First Login Applications\setup_silent.exe" 
        OnlyLaptops = $true 
    },
    [Program]@{
        Name = "Freshworks Agent"
        InstallerPath = "C:\group_policy_files\First Login Applications\setup_script\fs-windows-agent-2.7.0.msi" 
        ProgramPath = @("C:\Program Files (x86)\Freshdesk\Freshservice Discovery Agent\bin\FSAgentService.exe")
        Arguments = @("/quiet")
    },
    [Program]@{
        Name = "8x8 Work"
        InstallerPath = "C:\group_policy_files\First Login Applications\setup_script\work_7.0.5-3_64-bit.msi"
        ProgramPath = @("C:\Program Files\8x8 Inc\8x8 Work\8x8 Work.exe")
        Arguments = @("/quiet")
        Version = @("7.0.5.3")
    }
)

# if computer is a desktop (checking BatteryStatus is reliable for our current hardware, though may return false positives if we deploy desktops with uninterruptible power supplies)
[bool] $ComputerIsDesktop = (Get-WmiObject -Class Win32_Battery | Select-Object -ExpandProperty BatteryStatus) -eq $null

# ascii arrows for console output
$greenArrow = @{
  Object = [Char]0x25BA
  ForegroundColor = 'Green'
  NoNewLine = $true
}
$redArrow = @{
  Object = [Char]0x25BA
  ForegroundColor = 'Red'
  NoNewLine = $true
}
  
# displays bitlocker encryption progress bar until fully encrypted
function Wait-BitlockerEncryption ($Drive = 'C:', [int]$Interval = 1) {
    $ErrorActionPreference = 'Stop'
    do {
        Start-Sleep $Interval
        $manageBDEParams = @(
            '-status'
            $Drive
        )
        [String]$bdeResult = & manage-bde.exe @manageBDEParams
        $bdeResultRegex = 'Percentage Encrypted: +?(?<PercentComplete>\d+)'
        if ($bdeResult -match $bdeResultRegex) {
            [int]$PercentComplete = $Matches.PercentComplete
            $WriteProgressParams = @{
                Activity = 'Bitlocker Drive Encryption Status'
                Status = 'Encrypting'
                PercentComplete = $PercentComplete
            }
            Write-Progress @WriteProgressParams
        }
    } while ($PercentComplete -ne 100)

    Write-Progress -Activity 'Bitlocker Drive Encryption Status' -Completed
}

# given a process name, stops the process when it starts
function Wait-Kill ($program) {    
    Start-Job {
        do {
            Start-Sleep -Seconds 1
        } until (Get-Process $using:program -ErrorAction SilentlyContinue)
        Stop-Process -Name $using:program
    } | Out-Null
}

# given a process window name, stops the process when it starts
function Wait-KillWindow ($window) {    
    Start-Job {
        do {
            Start-Sleep -Seconds 1
        } until (Get-Process | Where-Object { $_.MainWindowTitle -eq $using:window } -ErrorAction SilentlyContinue)
        Get-Process | Where-Object { $_.MainWindowTitle -eq $using:window } | Stop-Process
    } | Out-Null
}

# verify connected to domain
function JoinedDomain () {
    return (gwmi win32_computersystem).partofdomain
}

# schedule task to start script automatically after next login
function ScheduleTask($ScriptCommand) {
    $TaskTrigger = New-ScheduledTaskTrigger -AtLogon
    $TaskTrigger.Delay = 'PT5S' # 5 second delay in ISO 8601 duration format
    $TaskAction = New-ScheduledTaskAction -Execute powershell.exe -Argument $ScriptCommand
    $TaskUserID = New-ScheduledTaskPrincipal -UserId Administrator -RunLevel Highest -LogonType Interactive
    Register-ScheduledTask -Force -TaskName setup_script -Action $TaskAction -Trigger $TaskTrigger -Principal $TaskUserID | Out-Null
}

function DeleteTask() {
    if (Get-ScheduledTask | Where-Object { $_.TaskName -eq "setup_script" }) {
        Write-Host "Deleting setup_script task"
        $error.clear()
        Unregister-ScheduledTask -TaskName "setup_script" -Confirm:$false
        if ($error) {
            taskschd.msc
            Write-Host @redArrow; Read-Host "Error deleting task, please delete the 'setup_script' task in Task Scheduler. Press Enter or close this window to exit"
        }
    }
}

function UpdateWindows($CheckOnly = $false, $AutoReboot = $false) {
    Set-ExecutionPolicy Bypass -Scope Process -force # allow import of PSWindowsUpdate module
    if (-not (Test-Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\PSWindowsUpdate")) {
        Copy-Item -Path "C:\group_policy_files\First Login Applications\setup_script\PSWindowsUpdate" -Destination "C:\Windows\System32\WindowsPowerShell\v1.0\Modules" -Recurse
    }

    Import-Module "C:\group_policy_files\First Login Applications\setup_script\PSWindowsUpdate\PSWindowsUpdate.dll"
    Import-Module "C:\group_policy_files\First Login Applications\setup_script\PSWindowsUpdate\PSWindowsUpdate.psm1"
    if ($CheckOnly) {
        Write-Host "Checking for Windows Updates"
        Get-WindowsUpdate | Out-Null
    } else { 
        Write-Host "Installing Windows Updates..."
        Install-WindowsUpdate -MicrosoftUpdate -AcceptAll -AutoReboot:$AutoReboot | Out-Null
    }
    
}

# given a window name, keeps window at foreground until closed
$FocusWindowByName = {
    param([String]$WindowName)
    Set-ExecutionPolicy Bypass -Scope Process -force # allow import of Wasp module

    # load Wasp module and Reflection dependency
    if (!(Get-module "Reflection")) {
        Import-Module "C:\group_policy_files\First Login Applications\setup_script\Wasp\Reflection.psm1"
    }
    if (!(Get-module "Wasp")) {
        Import-Module "C:\group_policy_files\First Login Applications\setup_script\Wasp\Wasp.psm1"
    }
    
    # save PID, as window names can change at runtime
    do {
        $processPID = Get-process | Where-Object { $_.MainWindowTitle -eq $WindowName } | Select-Object -ExpandProperty Id
    } while ($processPID -eq $null)

    # if process is not in foreground, bring to foreground
    $Process = Select-UIElement -PID $processPID
    while ($Process.WindowInteractionState -eq "ReadyForUserInteraction") {
        Start-Sleep -MilliSeconds 300
        $Process | Invoke-Window.SetWindowVisualState -state Normal # requires elevated permissions
    }
}


# rename computer, join domain, and reboot
function step1() {
    Write-Host "Closing rapid storage installer, microsoft edge, teams, and docvelocity installer..."
    Wait-Kill "Intel-Rapid-Storage-Technology-Driver_1NN1D_WIN_17.2.4.1011_A02_01"
    Wait-Kill "MicrosoftEdge"
    Wait-Kill "Teams"
    Wait-KillWindow "DocVelocity Desktop v.8.0 Setup"

    # get computer name
    Write-Host "Joining domain and changing PC name"
    if ($ComputerIsDesktop) {
        $WindowName = "Change Desktop Name"
        $PCNameSuffix = "-D"
    } else {
        $WindowName = "Change Laptop Name"
        $PCNameSuffix = "-L"
    }
    Start-Job -ScriptBlock $FocusWindowByName -ArgumentList $WindowName | Out-Null # keep asset id input window on top
    $credential = Get-Credential admin
    $PCNameSuffix = ("$env:computername" -split "-")[1] + $PCNameSuffix # extract service tag from old name
    Add-Type -AssemblyName 'Microsoft.VisualBasic'

    function RenameComputer($PCNameSuffix) { 
        $assetID = [Microsoft.VisualBasic.Interaction]::InputBox("Input the asset ID and press Enter to continue.",$WindowName)
        $NewName = ("{0}-{1}" -f $assetID,$PCNameSuffix).replace(' ','')
        if ($NewName -ne $env:computername) {
            Rename-Computer -DomainCredential $credential -NewName $NewName -WarningAction SilentlyContinue
        } else { Write-Host "Skipping name change. Computer name is already: $env:computername" }
        return $NewName
    }
    
    # error handling in case domain join fails
    function CancelDomainJoin($err) { Write-Host @redArrow; $err; if ( (Read-Host -Prompt "Press enter to retry domain join or enter [c] to cancel domain join") -eq 'c' ) { return $true }; return $false }
    $RemoveFromAD = {
        Import-Module "C:\group_policy_files\First Login Applications\setup_script\ActiveDirectory\Microsoft.ActiveDirectory.Management.dll"
        Import-Module "C:\group_policy_files\First Login Applications\setup_script\ActiveDirectory\Microsoft.ActiveDirectory.Management.resources.dll"
        $NewName = $null
        while ($NewName -eq $null) {
            $NewName = RenameComputer $PCNameSuffix
            $Message = "Are you sure you want to delete computer " + $NewName + " from the domain? Please ensure the asset ID you entered is correct, or you may affect another user on the domain!"
            if ( [Microsoft.VisualBasic.Interaction]::MsgBox($Message, 'YesNo,Exclamation', "WARNING!") -eq "Yes") {
                Remove-ADComputer -Identity $NewName -Credential admin
                Write-Host "$NewName has been removed from the DOMAIN.NET domain"
            } else { 
                $NewName = $null
            }
        }
    }

    # Connect to domain and change PC name
    :domain do { 
      try {
        if ($null -eq $NewName) {
            $NewName = RenameComputer $PCNameSuffix
        }

        $addComputerSplat = @{
          DomainName = "DOMAIN.NET"
          Force = $true
          ComputerName = $env:Computername
          Credential = $Credential
          Options = 'JoinWithNewName','AccountCreate'
          WarningAction = 'SilentlyContinue'
          ErrorAction = 'Stop'
        }
        Add-Computer @addComputerSplat
      } catch {
            $joined = $false 
            switch -regex ($_.Exception.Message) 
            {
                "unknown user name | Access is denied" { if (CancelDomainJoin $_) { break domain }; $credential = Get-Credential }
                "domain does not exist | No mapping between account names" { if (CancelDomainJoin $_) {break domain}; }
                "password is incorrect" { if (CancelDomainJoin $_) { break domain }; $credential = Get-Credential admin }
                "because it is already in that domain. | account already exists." { "Error: PC already exists in active directory"; 
                    if ((Read-Host -Prompt "Press enter to remove this computer from domain and retry domain join or enter [c] to cancel domain join") -eq 'c') 
                    { break domain }; Invoke-Command -ScriptBlock $RemoveFromAD }
                default { 'Unexpected error: ' + $_; if (CancelDomainJoin $_) { break domain }; $credential = Get-Credential; $NewName = $null }
            }
        }
    } until ($joined)

    # setup task to resume script after reboot
    ScheduleTask '-ExecutionPolicy ByPass -File "C:\group_policy_files\First Login Applications\setup_script\setup_script.ps1" -step 2'

    if (-not $error) { 
        Write-Host @greenArrow; "Computer will join domain after reboot and setup script will resume after Administrator login" 
    } else {
        Write-Host @redArrow; "Encountered errors in setup. Computer might join domain after reboot and setup script might resume after Administrator login, depending on severity of errors in red text above."
        if ( ([Microsoft.VisualBasic.Interaction]::MsgBox("Setup script encountered errors, reboot now?",'YesNo', "Setup Script Step 1 Complete") ) -eq "No") {
            Read-Host "Press Enter to Reboot"
        }
    }

    Restart-Computer
    Write-Host "Restarting computer..."
} 

# install software and windows updates
function step2($programs) {
    # delete task created in step 1
    DeleteTask

    Write-Host "Closing teams"
    Wait-Kill "Teams"

    # verify connected to domain
    if (JoinedDomain) { Write-Host @greenArrow; "DOMAIN.NET domain joined successfully" }

    # install programs
    foreach ($program in $programs) { $program.install() }

    Write-Host ""

    # verify programs installed
    foreach ($program in $programs) {
        $VersionString = if ($program.Version) {" " + $program.Version}
        if ( !($program.isInstalled())) {
            Write-Host @greenArrow; Write-Host $program.Name $VersionString "installed successfully!"
        } else { #retry installation 3 times
            $counter = 0
            while ($counter -le 3) {
                if ($program.isInstalled()) { 
                    Write-Host @greenArrow; Write-Host $program.Name $VersionString "installed successfully!"
                    break
                } else {
                    if ($counter -ge 1) {
                        Write-Host "$program.Name installation failed, reinstalling now"
                        $program.install
                    }
                }
                $counter = $counter + 1
            }
            if ($counter -eq 4) {
                "$program.Name installation failed, attempted to reinstall 3 times"
            }
            
          }
    }

    # check if Windows updates are available
    UpdateWindows $true

    if (!$ComputerIsDesktop) {
        # Encryption progress bar
        Wait-BitlockerEncryption
        Write-Host @greenArrow
        Write-Host "Hard Drive Encrypted 100%."
    }

    ScheduleTask '-ExecutionPolicy ByPass -File "C:\group_policy_files\First Login Applications\setup_script\setup_script.ps1" -step 3' # continue windows updates after reboot
    UpdateWindows $false $false # install Windows updates
    Write-Host @greenArrow
    Read-Host "Press Enter to reboot and continue Windows updates"
    Restart-Computer
} 

# finish windows updates
function step3() {
    Wait-Kill "Teams"
    # windows updates
    UpdateWindows $false $true
    
    # prompt to cancel windows update check on reboot
    Add-Type -AssemblyName 'Microsoft.VisualBasic'
    if ( ((Get-WindowsUpdate) -eq $null) -or ((Get-ScheduledTask | Where-Object { $_.TaskName -eq "setup_script" }) -and (([Microsoft.VisualBasic.Interaction]::MsgBox("Would you like to cancel automatic Windows Updates after administrator login?",'YesNo', "End Script") ) -eq "Yes")) ) {   
            Write-Host "Updating completed: deleting PSWindowsUpdate module"
            Remove-Item "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\PSWindowsUpdate" -Recurse -confirm:$false
            DeleteTask
            Write-Host "PC Setup Complete!

███████╗██╗███╗   ██╗██╗███████╗██╗  ██╗███████╗██████╗ 
██╔════╝██║████╗  ██║██║██╔════╝██║  ██║██╔════╝██╔══██╗
█████╗  ██║██╔██╗ ██║██║███████╗███████║█████╗  ██║  ██║
██╔══╝  ██║██║╚██╗██║██║╚════██║██╔══██║██╔══╝  ██║  ██║
██║     ██║██║ ╚████║██║███████║██║  ██║███████╗██████╔╝
╚═╝     ╚═╝╚═╝  ╚═══╝╚═╝╚══════╝╚═╝  ╚═╝╚══════╝╚═════╝ 
" 
            $Host.UI.ReadLine()
    } else { 
        Read-Host "Computer will resume updates after next login. Press Enter to reboot"
        Restart-Computer
    }

    
}



# keep screen on 
    $code=@' 
[DllImport("kernel32.dll", CharSet = CharSet.Auto,SetLastError = true)]
  public static extern void SetThreadExecutionState(uint esFlags);
'@

    $ste = Add-Type -memberDefinition $code -name System -namespace Win32 -passThru  
    $ES_CONTINUOUS = [uint32]"0x80000000" #Requests that the other EXECUTION_STATE flags set remain in effect until SetThreadExecutionState is called again with the ES_CONTINUOUS flag set and one of the other EXECUTION_STATE flags cleared.
    $ES_DISPLAY_REQUIRED = [uint32]"0x00000002" #Requests display availability (display idle timeout is prevented).
    $ES_SYSTEM_REQUIRED = [uint32]"0x00000001" #Requests system availability (sleep idle timeout is prevented).
    $ste::SetThreadExecutionState($ES_SYSTEM_REQUIRED -bor $ES_DISPLAY_REQUIRED -bor $ES_CONTINUOUS )

# process script flags
if ($step -eq 1) {
    if (JoinedDomain) {
        Add-Type -AssemblyName 'Microsoft.VisualBasic'
        if ( ([Microsoft.VisualBasic.Interaction]::MsgBox("$env:computername is already connected to the DOMAIN.NET Domain. Would you like to skip to step 2: software installations?",'YesNo,Question', "Domain Already Joined") ) -eq "Yes") {
            step2($programs)
            break
        }
    }
    step1
} elseif($step -eq 2) { 
    step2($programs) 
} elseif($step -eq 3) { 
    step3
} else {
    Read-Host "ERROR: Invalid step number. Please run script with a '-step 1', '-step 2', or '-step 3' flag. Press Enter to exit."
}
